<?php

namespace App\Factories;

use App\Models\Feedback;
use App\Services\Storage\FeedbackStorageInterface;

class FeedbackFactory
{
    public static function create(array $data, FeedbackStorageInterface $storage)
    {
        $feedback = new Feedback($data);
        $storage->store($feedback);
    }
}
