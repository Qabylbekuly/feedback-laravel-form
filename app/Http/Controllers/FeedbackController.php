<?php

namespace App\Http\Controllers;

use App\Factories\FeedbackFactory;
use App\Http\Requests\StoreFeedbackRequest;
use App\Services\Storage\DatabaseFeedbackStorage;
use App\Services\Storage\FileFeedbackStorage;

class FeedbackController extends Controller
{
    public function store(StoreFeedbackRequest $request)
    {
        $data = $request->validated();

        // Сохранение в базу данных
        FeedbackFactory::create($data, new DatabaseFeedbackStorage());

        // Сохранение в файл
        FeedbackFactory::create($data, new FileFeedbackStorage());

        return response()->json(['message' => 'Заявка принята']);
    }
}
