<?php

namespace App\Services\Storage;

use App\Models\Feedback;

class FileFeedbackStorage implements FeedbackStorageInterface
{
    public function store(Feedback $feedback)
    {
        $file = storage_path('feedbacks.txt');
        file_put_contents($file, $feedback->toJson() . PHP_EOL, FILE_APPEND);
    }
}
