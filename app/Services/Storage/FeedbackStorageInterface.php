<?php

namespace App\Services\Storage;

use App\Models\Feedback;

interface FeedbackStorageInterface
{
    public function store(Feedback $feedback);
}
